package com.sbutler.taas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.virtualbox_5_1.VirtualBoxManager;


@SpringBootApplication
public class ApplicationTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationTest.class);

    public static void main(String[] args) {
        SpringApplication.run(ApplicationTest.class, args);
    }
    
    @Bean()
    VirtualBoxManager init() {
    	logger.info("Creating VirtualBoxManager(test)");
    	VirtualBoxManager mgr = VirtualBoxManager.createInstance(null);
		return mgr;
    }
    
}

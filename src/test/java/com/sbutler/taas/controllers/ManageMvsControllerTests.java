package com.sbutler.taas.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.sbutler.taas.ApplicationTest;
import com.sbutler.taas.exceptions.VmNotFoundException;
import com.sbutler.taas.services.ManageVms;
import com.sbutler.taas.services.VmStatus;
import com.sbutler.taas.services.VmValidation;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=ApplicationTest.class)
@AutoConfigureMockMvc
public class ManageMvsControllerTests {

    @Autowired
    private MockMvc mockMvc;
	
    @MockBean
	private VmValidation validationMock;
    
    @MockBean
	private ManageVms manageVms;
    
    @Test
    public void createdVmReturnsStarted() throws Exception {
    	final String mac = "1234567890AV";

    	doNothing().when(validationMock).exists(mac);
    	
    	when(manageVms.getStatus(mac)).thenReturn(VmStatus.STARTED);
    	
        this.mockMvc.perform(get("/api/vm/status/" + mac)).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(VmStatus.STARTED.toString()));
    }

    @Test
    public void nonExistingMacReturns404() throws Exception {
    	final String mac = "1234567890AV";
    	doThrow(new VmNotFoundException(mac)).when(validationMock).exists(mac);
    	
        this.mockMvc.perform(get("/api/vm/status/" + mac)).andDo(print()).andExpect(status().isNotFound())
                .andExpect(content().string("This VM for this MAC address was not found " + mac));
    }
}

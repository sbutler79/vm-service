package com.sbutler.taas.services;

import static org.mockito.Mockito.when;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sbutler.taas.ApplicationTest;
import com.sbutler.taas.data.VirtualMachine;
import com.sbutler.taas.data.VirtualMachineRepository;
import com.sbutler.taas.exceptions.MacExistsException;
import com.sbutler.taas.exceptions.InvalidVmStateException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=ApplicationTest.class)
public class VmValidationTest {
	
	@MockBean
	private VirtualMachineRepository vmRepository;
	
	@Autowired
	private VmValidation validation;
	
	@Rule
	public ExpectedException thrown= ExpectedException.none();
	
	@Test
	public void readyForStartAlreadyExisting() {
		final String mac = "1234567890AV";
		when(vmRepository.findByMacAddress(mac)).thenReturn(new VirtualMachine(mac));
		thrown.expect(MacExistsException.class);
		
		validation.readyForStart(mac);
	}
	
	@Test
	public void readyForStartNotExisting() {
		final String mac = "1234567890AV";
		when(vmRepository.findByMacAddress(mac)).thenReturn(null);
		
		validation.readyForStart(mac);
	}
	
	@Test
	public void readyForStopNotStarted() {
		final VmStatus state = VmStatus.STOPPED;
		final String mac = "1234567890AV";
		VirtualMachine vm = new VirtualMachine(mac);
		vm.setStatus(state);
		when(vmRepository.findByMacAddress(mac)).thenReturn(vm);
		
		thrown.expect(InvalidVmStateException.class);
		thrown.expectMessage("This VM cannot be shutdown - it is not in the Started state. It is in state: " + state);
		
		validation.readyForStop(mac);
	}
	
	@Test
	public void readyForStopValid() {
		final String mac = "1234567890AV";
		VirtualMachine vm = new VirtualMachine(mac);
		vm.setStatus(VmStatus.STARTED);
		when(vmRepository.findByMacAddress(mac)).thenReturn(vm);
		
		validation.readyForStop(mac);
	}

}

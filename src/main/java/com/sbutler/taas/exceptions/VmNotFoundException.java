package com.sbutler.taas.exceptions;

public class VmNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -5697937768672153202L;

	public VmNotFoundException(String macAddress) {
		super("This VM for this MAC address was not found " + macAddress);
	}
	
}

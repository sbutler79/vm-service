package com.sbutler.taas.exceptions;

public class InvalidVmStateException extends RuntimeException {

	private static final long serialVersionUID = 2972672740279477820L;

	public InvalidVmStateException(String message, String macAddress) {
		super(message + " Mac Address: " + macAddress);
	}
	
}

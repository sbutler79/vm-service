package com.sbutler.taas.exceptions;

public class MacExistsException extends RuntimeException {

	private static final long serialVersionUID = 2152918434308102994L;

	public MacExistsException(String macAddress) {
		super("This Mac address is already in use " + macAddress);
	}
	
}

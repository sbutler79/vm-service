package com.sbutler.taas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.virtualbox_5_1.VirtualBoxManager;


@SpringBootApplication
@EnableAsync
public class Application extends SpringBootServletInitializer {
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
    @Value("${virtualbox.webserver.url}")
    private String virtualboxUrl;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
    
    @Bean()
    VirtualBoxManager init() {
    	logger.info("Creating VirtualBoxManager");
    	VirtualBoxManager mgr = VirtualBoxManager.createInstance(null);
		mgr.connect(virtualboxUrl, "", "");
		return mgr;
    }
    
}

package com.sbutler.taas.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbutler.taas.data.VirtualMachine;
import com.sbutler.taas.data.VirtualMachineRepository;
import com.sbutler.taas.exceptions.InvalidVmStateException;
import com.sbutler.taas.exceptions.MacExistsException;
import com.sbutler.taas.exceptions.VmNotFoundException;

@Service
public class VmValidationImpl implements VmValidation {
	@Autowired
	private VirtualMachineRepository vmRepository;
	
	public void readyForStart(String macAddress) {
		if (vmRepository.findByMacAddress(macAddress) != null) {
			// define my own custom exception here
			throw new MacExistsException("This mac address is already in use.  Please try another.");
		}
	}

	@Override
	public void readyForStop(String macAddress) {
    	// Address must exist and be STARTED
		VirtualMachine vm = vmRepository.findByMacAddress(macAddress);
		if (vm == null) {
			throw new VmNotFoundException(macAddress);
		}
		else if (!vm.getStatus().equals(VmStatus.STARTED)) {
			throw new InvalidVmStateException("This VM cannot be shutdown - it is not in the Started state. It is in state: " + vm.getStatus(), macAddress);
		}
		
	}

	@Override
	public void exists(String macAddress) {
		if (vmRepository.findByMacAddress(macAddress) == null) {
			throw new VmNotFoundException(macAddress);
		}
		
	}
}

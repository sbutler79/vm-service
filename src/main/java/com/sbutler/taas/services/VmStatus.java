package com.sbutler.taas.services;

public enum VmStatus {
	BOOTING, STARTED, SHUTTING_DOWN, STOPPED, NOT_FOUND
}

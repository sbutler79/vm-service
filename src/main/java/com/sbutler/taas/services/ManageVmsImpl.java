package com.sbutler.taas.services;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.virtualbox_5_1.CleanupMode;
import org.virtualbox_5_1.IAppliance;
import org.virtualbox_5_1.IConsole;
import org.virtualbox_5_1.IMachine;
import org.virtualbox_5_1.IMedium;
import org.virtualbox_5_1.IProgress;
import org.virtualbox_5_1.ISession;
import org.virtualbox_5_1.IVirtualBox;
import org.virtualbox_5_1.LockType;
import org.virtualbox_5_1.VirtualBoxManager;

import com.sbutler.taas.data.VirtualMachine;
import com.sbutler.taas.data.VirtualMachineRepository;

@Service
public class ManageVmsImpl implements ManageVms {

	private static final Logger logger = LoggerFactory.getLogger(ManageVmsImpl.class);
	
	@Autowired
	private VirtualBoxManager mgr;
	
	@Autowired
	private VirtualMachineRepository vmRepository;
	
    @Value("${vm.base.image.location}")
    private String vmImageLocation;
	
	@Override
	public VmStatus getStatus(String macAdress) {
		VirtualMachine vm = vmRepository.findByMacAddress(macAdress);
		
		if (vm != null) {
			return vm.getStatus();
		}
		else {
			return VmStatus.NOT_FOUND;
		}
		
	}

	@Override
	public void startVm(String macAddress) {
		logger.info("Starting VM with mac address: " + macAddress);
		// vm status
		VirtualMachine vm = new VirtualMachine(macAddress);
		vm.setStatus(VmStatus.BOOTING);
		vmRepository.save(vm);
		
        IVirtualBox vbox = mgr.getVBox();
        // Create Appliance
        IAppliance appliance = vbox.createAppliance();
        IProgress p1 = appliance.read(vmImageLocation);
        p1.waitForCompletion(10000);
        
        // Interpret
        appliance.interpret();
        
        // Import
        IProgress p2 = appliance.importMachines(null);
        p2.waitForCompletion(1000 * 60 * 10);
        
        // Find machine
        List<String> machines = appliance.getMachines();
        IMachine machinenew =  vbox.findMachine(machines.get(0));
        
        // Set MAC
        ISession session = mgr.getSessionObject();
        machinenew.lockMachine(session, LockType.Write);
        IMachine mutable = session.getMachine();
        mutable.getNetworkAdapter((long) 0).setMACAddress(macAddress);
        mutable.saveSettings();
        session.unlockMachine();

        // Start machine
        IProgress p = machinenew.launchVMProcess(session, "gui", "");
        p.waitForCompletion(10000);
        
        // Update status
		vm.setStatus(VmStatus.STARTED);
		vmRepository.save(vm);
		logger.info("Started VM with mac address: " + macAddress);
		getMachine(macAddress);  // TODO: remove
	}

	
	private IMachine getMachine(String macAddress) {
		logger.debug("Looking for VM: " + macAddress);
		IVirtualBox vbox = mgr.getVBox();	
		List<IMachine> machines = vbox.getMachines();
		for  (IMachine machine: machines) {;
			if (Objects.equals(machine.getNetworkAdapter((long) 0).getMACAddress(), macAddress)) {
				return machine;
			}
		}
		logger.error("VM not found: " + macAddress);
		return null;
	}

	@Override
	public void stopVm(String macAddress) {
		logger.info("Stopping VM with mac address: " + macAddress);
		VirtualMachine vm = vmRepository.findByMacAddress(macAddress);
		if (vm == null) {
			// Throw some custom exception
			return;
		}
		vm.setStatus(VmStatus.SHUTTING_DOWN);
		vmRepository.save(vm);
		
		IMachine machine = getMachine(macAddress);
		// Machine is not found, disable in repo (DB status does not match machines in virtualbox webserver)
		if (machine == null) {
			vm.setStatus(VmStatus.NOT_FOUND);
			vmRepository.save(vm);
			return;
		}
		
		// Lock and powerdown machine
        ISession session = mgr.getSessionObject();
        machine.lockMachine(session, LockType.Shared);
        IConsole console = session.getConsole();
        IProgress pr = console.powerDown();
        pr.waitForCompletion(20000);
        
        // Unlock machine
        mgr.closeMachineSession(session); 
        mgr.waitForEvents(1000 * 20); // Wait for session to close
    	
        // unregister machine and delete from HD
        List<IMedium> mediumObjs = machine.unregister(CleanupMode.Full);
        IProgress p2 = machine.deleteConfig(mediumObjs);
        p2.waitForCompletion(20000);

		vm.setStatus(VmStatus.STOPPED);
		vmRepository.save(vm);
	}

	
}

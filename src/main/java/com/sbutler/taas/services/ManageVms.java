package com.sbutler.taas.services;

import org.springframework.scheduling.annotation.Async;

public interface ManageVms {

	public VmStatus getStatus(String macAdress);
	
	@Async
	public void startVm(String macAdress);
	
	@Async
	public void stopVm(String macAdress);
	
}

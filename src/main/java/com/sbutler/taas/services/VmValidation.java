package com.sbutler.taas.services;

public interface VmValidation {

	public void readyForStart(String macAddress);
	
	public void readyForStop(String macAddress);
	
	public void exists(String macAddress);
	
}

package com.sbutler.taas.controllers;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sbutler.taas.resolvers.UpperCase;
import com.sbutler.taas.services.ManageVms;
import com.sbutler.taas.services.VmStatus;
import com.sbutler.taas.services.VmValidation;

@RestController
@RequestMapping("/api/vm")
@Validated
public class ManageVmsController {
	
	@Autowired
	private ManageVms manageVms;
	
	@Autowired
	private VmValidation validation;
    
    @RequestMapping(value = "/start/{macAddress}", method = RequestMethod.POST)
    public ResponseEntity<?> startVm(@Valid @NotNull
    		@Size(max = 12, min = 12, message = "Mac address should be 12 character") 
    		@UpperCase("macAddress") String macAddress) {
    
		validation.readyForStart(macAddress);
    	
    	// Kick off process
    	manageVms.startVm(macAddress);
        return ResponseEntity.accepted().build();
    }
    
    @RequestMapping(value = "/stop/{macAddress}", method = RequestMethod.POST)
    public ResponseEntity<?> stopVm(@UpperCase("macAddress") String macAddress) {
    	
    	validation.readyForStop(macAddress);
    	
    	// Kick off process
    	manageVms.stopVm(macAddress);
        return ResponseEntity.accepted().build();
    }
    
    @RequestMapping(value = "/status/{macAddress}", method = RequestMethod.GET)
    public ResponseEntity<?> getStatus(@UpperCase("macAddress") String macAddress) {
    	validation.exists(macAddress);
    	// Kick off process
    	VmStatus status = manageVms.getStatus(macAddress);
        return ResponseEntity.ok(status.toString());
    }
}

package com.sbutler.taas.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sbutler.taas.services.VmStatus;

@Entity
public class VirtualMachine {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	private VmStatus status;
	@Column(unique = true)
	private String macAddress;
	
	protected VirtualMachine() {}
	
	public VirtualMachine(String macAddress) {
		this.macAddress = macAddress;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public VmStatus getStatus() {
		return status;
	}

	public void setStatus(VmStatus status) {
		this.status = status;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	@Override
	public String toString() {
		return "VirtualMachine [id=" + id + ", status=" + status + ", macAddress=" + macAddress + "]";
	}
	
}

package com.sbutler.taas.data;

import org.springframework.data.repository.CrudRepository;

public interface VirtualMachineRepository extends CrudRepository<VirtualMachine, Long> {

	VirtualMachine findByMacAddress(String macAdress);
	
}

# Testbirds Java Challenge

(Prerequisites: you have installed JDK 8 or greater and maven)  
** Usage **:  
 
* Clone repo   
* Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)  
* Download a Windows 7 [VM](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/)  
* Disable Virtualbox authentication
~~~~ 
> VBoxManage setproperty websrvauthlibrary null  
~~~~ 
* Add location for your windows 7 .ovf file in application.properties (src/main/resources)  
* Deploy included virtual box client jar to your local repo
~~~~ 
> mvn install:install-file -Dfile="<path_to_this_repo>/testbirds-exercise/repo/vboxjws.jar"
 -DgroupId=org.virtualbox -DartifactId=vboxjws -Dversion=5.1.22 -Dpackaging=jar  
~~~~
(I couldn't find a public location for the newer artifact)  

* Build war  
~~~~
> mvn clean install -DskipTests  
~~~~
* Start vbox web server (This is included in the installation from step 2) 
~~~~ 
> <virtual_box_install_dir>/./vboxwebsrv -v  
~~~~

* Deploy war on tomcat server  

Create a VM:
POST: http://localhost:8080/<your_war_name>/api/vm/stop/<mac_address> 

Caveats:  
1. Test case coverage is minimal  
2. Only basic exception handling is covered  
3. Authentication not covered